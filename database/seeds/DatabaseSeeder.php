<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert(
            [
                'name'           => 'admin',
                'email'          => 'admin@root.com',
                'password'       => bcrypt("admin_root09"), // secret
                'remember_token' => str_random(10),
            ]);

        DB::table('schools')->insert(
            [
                'name'    => 'Bhairab secondary school',
                'address' => 'Junichadi-2, Marjkot, Jajarkot',
                'logo'    => "img/logo/Bagh-Bhairab-Secondary-School.jpg"
            ]);
    }
}
