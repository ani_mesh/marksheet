@extends('layouts.new')
@section('content')
<div class="banner">
  <h2>
  <a href="#">Home</a>
  <i class="fa fa-angle-right"></i>
  <span>Edit class</span>
  </h2>
</div>
<!--//banner-->

<div class="content-top">
  
  
  
  <div class="grid-form1">
    <form class="form-horizontal" action="{{route('class.update',['class_id'=>$class->id])}}" method="post">
      {{ csrf_field() }}
     <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label hor-form">Class Name:</label>
    <div class="col-sm-10">
      <label class="control-label hor-form">{{$class->name}}</label>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label hor-form">Class Teacher:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="" name="teacher" value="{{$class->class_teacher}}" required>
    </div>
  </div>
      <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Save</button>
    </div>
  </div>
      
    </form>
  </div>
  
</div>

@stop
@section('style')

@stop