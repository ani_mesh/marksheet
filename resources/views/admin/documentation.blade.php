<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Marksheet Documentation - a documentation page for using this app</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/scribbler-global.css')}}">
    <link rel="stylesheet" href="{{asset('css/scribbler-doc.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.js"></script>
    <style type="text/css">
    	.login{
    		height: 400px;
    		width: 470px;
    	}
    	html {
 		 scroll-behavior: smooth;
		}
		* {
		  box-sizing: border-box;
		}

		.columna {
		  float: left;
		  width: 50%;
		  padding: 5px;
		}

		.columnb {
		  float: left;
		  width: 33.33%;
		  padding: 5px;
		}

		/* Clearfix (clear floats) */
		.rowa::after {
		  content: "";
		  clear: both;
		  display: table;
		}
		.classImg{

    		height: 450%;
		}
    </style>
    
  </head>
  <body>
    <div class="doc__bg"></div>
    <nav class="header">
      <h1 class="logo">App <span class="logo__thin">Doc</span></h1>
      <ul class="menu">
        <div class="menu__item toggle"><span></span></div>
        <li class="menu__item"><a href="https://github.com/AnimeshPandey123" target="_blank" class="link link--dark"><i class="fa fa-github"></i> Github</a></li>
        <li class="menu__item"><a href="{{route('admin')}}" class="link link--dark"><i class="fa fa-home"></i> Home</a></li>
      </ul>
    </nav>
    <div class="wrapper">
      <aside class="doc__nav">
        <ul>
          <li class="js-btn selected">Get Started</li>
          <li class="js-btn">Configuration</li>
          <li class="js-btn">Usage
          		
		<li class="">
			<a href="#sidebar">SideBar</a>
		</li>
		<li class="">
			<a href="#class">Class</a>
		</li>
		<li class="">
			<a href="#student">Student</a>
		</li>
		<li class="">
			<a href="#marks">Marks</a>
		</li>
          		
          	</li>
          	

        </ul>
      </aside>
      <article class="doc__content">
        <section class="js-section">
          <h3 class="section__title">Get Started</h3>
          <p>Learn how to use this applications with these easy steps</p>
          <h3 class="section__title">Installation</h3>
          <div>
          	<p>First login with given credentials in this login screen.</p>
          	<img class="login" src="{{asset('documentation/login.png')}}">
          </div>
        </section>
        <section class="js-section">
          <h3 class="section__title">Configuration</h3>
          <p>Learn how to configure settings for this application. You need to firstly create academic years, classes, subjects for the classes and Terminals</p>
          <table id="">
            <tr>
              <th>Features</th>
              <th>Description</th>
              <th>Values</th>
            </tr>
            <tr>
              <td>Academic Year</td>
              <td>Create academic year like 2076, 2077, 2078, etc. This is for get results for specific year</td>
              <td>Name</td>
            </tr>
            <tr>
              <td>Classes</td>
              <td>Create classes like 7, 6, 5 etc. You can also add class teacher.</td>
              <td>Name and class teacher</td>
            </tr>
            <tr>
              <td>Terminal</td>
              <td>You can also add terminals like first, second and third or final.</td>
              <td>Name of the terminal</td>
            </tr>
            <tr>
              <td>Subject</td>
              <td>You can also add subjects like Nepali, English, etc. </td>
              <td>Name of subject, class, marks and credit hours</td>
            </tr>
            <tr>
              <td>Students</td>
              <td>You can also add students for a specific class </td>
              <td>Name of student, Roll Number, First Name, Middle Name, Last Name, BirthDate in A.D. and BirthDate in B.S.</td>
            </tr>
          </table>
          <p>These are all the sections you have to add before you begin using the application.</p>
          <hr />
        </section>
        <section class="js-section">
          <h3 class="section__title">Usage</h3>
          <div id="sidebar">
          	<h4>Sidebar</h4>
          	<p>You can get to all the features from this side panel.</p>
          	<div class="rowa" id="sidePanel">
			  <div class="columna">
			    <img class="" id="test" src="{{asset('documentation/sidebar.png')}}" alt="create" style="width:100%">
			  </div>
			</div>
          </div>
          <div id="class">
          	<h4>Class</h4>
          	<p>
          		You can add your classes here and also view them. As you can see you can also add students and view students in this class.
          	</p>
          	<div class="rowa" id="classImage">
			  <div class="columna">
			    <img class="classImg" id="test" src="{{asset('documentation/createclass.png')}}" alt="create" style="width:100%">
			  </div>
			  <div class="columna">
			    <img class="classImg" src="{{asset('documentation/viewclass.png')}}" alt="view" style="width:100%">
			  </div>
			</div>
          </div>
         
          <hr/>
          <div id="student">
          	<h4>Students</h4>
          	<p>
          		You can also add students to a specific class and academic year.
          		You can also view these students to class. 
          		You have to create class and academic year before adding students
          	</p>
          	<div class="rowa" id="stuImage">
			  <div class="columnb">
			    <img class="" id="test" src="{{asset('documentation/addStudent1.png')}}" alt="create" style="width:100%">
			  </div>
			  <div class="columna">
			    <img class="" src="{{asset('documentation/addstudent2.png')}}" alt="view" style="width:100%">
			  </div>
			   <div class="columna">
			    <img class="" src="{{asset('documentation/viewStudent.png')}}" alt="view" style="width:100%">
			  </div>
			</div>
          </div>
          <div id="marks">
          		<h4>Marks</h4>
          		<p>You can add marks of the students and view the pdf of those marks.
          			You need to add class, subject, students and academic year before adding the marks.
          		</p>
          		<div class="rowa" id="markImage">
			  <div class="columna">
			    <img class="" id="test" src="{{asset('documentation/addMark1.png')}}" alt="create" style="width:100%">
			  </div>
			  <div class="columna">
			    <img class="" src="{{asset('documentation/addMark2.png')}}" alt="view" style="width:100%">
			  </div>

			  <p>You can also view marks, update them and view marksheet</p>
			  <div class="rowb" id="viewmarkImage">
			  <div class="columna">
			    <img class="" id="test" src="{{asset('documentation/viewMark1.png')}}" alt="create" style="width:100%">
			  </div>
			  <div class="columna">
			    <img class="" src="{{asset('documentation/viewMark2.png')}}" alt="view" style="width:100%">
			  </div>
			  <div class="columna">
			    <img class="" src="{{asset('documentation/updateMark.png')}}" alt="view" style="width:100%">
			  </div>
			   <div class="columna">
			    <img class="" src="{{asset('documentation/marksheet1.png')}}" alt="view" style="width:100%">
			  </div>
			</div>


          </div>
          </div>
        </section>
        
      </article>
    </div>

    <footer class="footer">Marksheet is an app for creating marksheet for schools.</footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <script src="{{asset('js/scribbler.js')}}"></script>
    <script type="text/javascript">
    	 $(document).ready(function(){
    		// View a list of images
			const classImage = new Viewer(document.getElementById("classImage"));
			const studentImage = new Viewer(document.getElementById("stuImage"));
			const markImage = new Viewer(document.getElementById("markImage"));
			const viewmarkImage = new Viewer(document.getElementById("viewmarkImage"));
    	});
    	
    </script>
  </body>
</html>