<!DOCTYPE html>
<html>
<head>
	<title>Marksheet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
	<style type="text/css">
body{
    border:2px solid black;
}
.side{
	margin-left: 7px;
	margin-right: 7px;
}
.mark th,.mark td,{
    border: 1px solid black;
    border-spacing: 0em;
}
.mark th {
padding-bottom: 25px;
/*margin: 8px;*/
/*height:10px; */
   /* border: 1px solid black;
    border-spacing: 0em;
*/    

}
.foot{
	 border-top: 1px solid black;
    border-spacing: 0em;
 
}
.wrapper {
    
    margin: 0 auto;
    border: 1px solid black;
    height: 10%;
}

#block1 {
    float:left;
}

#block2
{
    float:right;
    width: 180px;
}
.result{
    border: 1px solid black;
    text-align: center;
}
 .result td {
width: 100%;
 border: 0.1px solid black;
    

}
.remarks{
	border: 1px solid black;
    text-align: center;
    height: 20%;
}

hr.line1{
	margin-left: 1px;
  border:none;
  border-top:1px dotted;
  color:#000;
  background-color:#fff;
  height:1px;
  width:50%;
}
hr.line2{
	margin-left: 1px;
  border:none;
  border-top:1px dotted;
  color:#000;
  background-color:#fff;
  height:1px;
  width:50%;
}
hr.line3{
	margin-left: -2px;
  border:none;
  border-top:1px dotted;
  color:#000;
  background-color:#fff;
  height:1px;
  width:50%;
}
hr.line4{
	margin-left: 2px;
  border:none;
  border-top:1px dotted #000;
  color:#000;
  background-color:#fff;
  height:1px;
  width:30%;
}

  #watermark {
   
  }
	</style>

</head>
<body>
<img id="watermark" src="{{ public_path('img/government.png') }}" style=" position: fixed;
    
    width: 100%;
    opacity: .1;
    background-repeat: repeat repeat !important;
    z-index: -1000;
    display: block;">
  

 
<div>


<img style="float: left; margin-left:6px;" src="{{ public_path('img/government.png') }}" width="100px" height="100px"><br>
<div style=" text-align: center;">
<label style="margin: 0; font-size: 16px;">Bhairab Secondary School</label>
<br>
<label style="margin: 0; font-size: 14px">OFFICE OF THE MUNICIPAL EXECUTIVE</label><br>
<label style="margin: 0; font-size: 14px">JUNICHADI 2, MAJKOT JAJARKOT</label><br>
<label style="margin: 0; font-size: 14px">KARNALI PROVINCE,NEPAL</label><br>
<label style="margin: 0; font-size: 22px;">BASIC LEVEL EDUCATION EXAMINATION</label>
<h4 style="margin: 0;">2074</h4>
<h5 style="margin: 0; font-size: 15px">GRADE-SHEET</h5>
</div>
<br><br>
<div class="side">
<div style="display: inline-block; font-size: 90%; margin: 7px;">THE GRADE (S) SECURED BY  </div><br>
<div  style="display: inline-block; font-size: 90%; margin: 4px;">DATE OF BIRTH (BS)...............................................................................(AD)................................................................... </div>
<div>ROLL</div><br>
<div >OF...........................................................................................................................................................................</div>
<div>IN THE ANNUAL BASIC LEVEL EDUCATION EXAMINATION (GRADE-8) 2074 ARE GIVEN BELOW.</div>
</div>
<br>
<table id="mark" class="mark" width="98%" cellspacing="0" style="margin-left: 6px; ">
<thead>
	<tr>
		<th rowspan="2">S.N.</th>
		<th rowspan="2">SUBJECTS</th>
		<th rowspan="2">CREDIT HOUR</th>
		<th colspan="2" rowspan="1" height="0">OBTAINED GRADE</th>
		<th rowspan="2">FINAL GRADE</th>
		<th rowspan="2">GRADE POINT</th>
		<th rowspan="2">REMARKS</th>
	</tr>
	<tr>
		<th rowspan="1">TH</th>
		<th rowspan="1">PR</th>
	</tr>

</thead>

	<tbody>
	
		<tr>
			
			<td height=""></td>
			<td style="text-align: center;">1</td>
			<td style="text-align: center;">1</td>
			<td style="text-align: center;">1</td>
			<td  style="text-align: center;">1</td>
			<td style="text-align: center;"></td>
			<td style="text-align: center;"></td>
			<td style="text-align: center;"></td>
			
		</tr>
			
	
	</tbody>
	<tfoot>	
		<tr >
		<td style="text-align: left;" colspan="4" class="foot"><span>
			<label>AVRAGE (GPA)</label></span></td>
			<td style="text-align: center;" colspan="3" class="foot">GRADE POINT</td>
			<td style="text-align: center;" class="foot"></td>
		</tr>
		</tfoot>
</table>	
<br>
<br>
<br>
<br>
<div>
	<table width="105%" style="margin-left: 6px">
		<tr>
			<td style="text-align: justify;">
				<hr class="line1">
			<label>PREPARED BY</label><br>
			<label>(HEAD TEACHER) </label>
			</td>
			<td>
				<hr class="line2">
			<label>REFERRED BY</label><br>
			<label>(RESOURCE PERSON)</label>
			</td>
			<td>
			<hr class="line3">
				<label>CHECKED BY</label><br>
			<label>(SECTION OFFICER)</label>
			</td>
			<td valign="bottom">
			<hr class="line4">
				<label>APPROVED BY</label><br>
				<label>(CHIEF ADMINISTRATIVE <br> OFFICER)</label>
			</td>
		</tr>
	</table>
</div>


</div>

<br>
<br>
<br>
<br>



</body>
</html>