<!DOCTYPE html>
<html>
<head>
	<title>Marksheet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
	<style type="text/css">
	body{
border:2px solid black;
		}
		.side{
		margin-left: 7px;
		margin-right: 7px;
		}
		.mark{
		border: 1px solid black;
		
		}
		.mark th,.mark td,{
		border: 1px solid black;
		border-spacing: 0em;
		}
		.mark th {
		padding-bottom: 25px;
		/*margin: 8px;*/
		/*height:10px; */
		/* border: 1px solid black;
		border-spacing: 0em;
		*/
		}
		.mark td:last-child{
   		 	border:none;
		}

		.foot{
		border-top: 1px solid black;
		border-spacing: 0em;
		
		}
		.wrapper {
		
		margin: 0 auto;
		border: 1px solid black;
		height: 10%;
		}
		
		hr.line1{
		margin-left: 1px;
		border:none;
		border-top:1px dotted;
		color:#000;
		background-color:#fff;
		height:1px;
		width:50%;
		}
		hr.line2{
		margin-left: 1px;
		border:none;
		border-top:1px dotted;
		color:#000;
		background-color:#fff;
		height:1px;
		width:50%;
		}
		hr.line3{
		margin-left: -2px;
		border:none;
		border-top:1px dotted;
		color:#000;
		background-color:#fff;
		height:1px;
		width:50%;
		}
		hr.line4{
		margin-left: 2px;
		border:none;
		border-top:1px dotted #000;
		color:#000;
		background-color:#fff;
		height:1px;
		width:30%;
		}
		#watermark {
		
		}

	</style>
</head>
<body style="background: url('/img/back4.jpg');
    
    background-repeat: repeat">
<div>
@foreach($students as $student)
<div style="page-break-inside: avoid">
<div>
@if($school)
@if($school->logo)
	@if(file_exists(public_path($school->logo)))
			<img style="float: left; margin-left:5px;" src="{{ public_path($school->logo) }}" width="100px" height="100px"><br>
	@endif
	@endif
@endif
			<div style=" text-align: center;">
				
				<label style="margin: 0; font-size: 14px">OFFICE OF THE MUNICIPAL EXECUTIVE</label><br>
				<label style="margin: 0; font-size: 22px;">@if($school){{strtoupper($school->name)}}@endif</label><br>
				<label style="margin: 0; font-size: 14px">@if($school){{strtoupper($school->address)}}@endif</label><br>

				
				<h4 style="margin: 0;">{{$student['student']->academicyear->year}}</h4>
				<h5 style="margin: 0; font-size: 15px; text-decoration: underline; margin-left: 110px;">GRADE-SHEET</h5>
			</div>
			<br><br>
			<div class="side">
				<div style="display: inline-block; font-size: 90%; margin: 7px;">THE GRADE (S) SECURED BY <label style=" border-bottom: 1px dotted #000;
					text-decoration: none;">
					{{strtoupper($student['student']->firstname)}} {{strtoupper($student['student']->middlename)}} {{strtoupper($student['student']->lastname)}}
				</label>  </div><br>
				<div  style="display: inline-block; font-size: 90%; margin: 5px;">DATE OF BIRTH  (BS)&nbsp; &nbsp;{{$student['student']->birthdate_bs}}<label style="margin-left: 225px;">(AD)&nbsp; &nbsp;{{$student['student']->birthdate_ad}}</label></div>
				<div style="margin: 9px;">ROLL NO. &nbsp; &nbsp;{{$student['student']->rollno}}<label style="margin-left: 245px;"> SYMBOL NO:.........................................................</label></div>
				<div style="margin: 6px;">OF THE {{strtoupper($terminal->term)}} TERMINAL</div>
				<div style="margin: 6px">IN THE ANNUAL LEVEL EDUCATION EXAMINATION (Grade {{$class->name}}) {{$student['student']->academicyear->year}} ARE GIVEN BELOW.</div>
			</div>
			<table id="mark" class="mark" width="98%" cellspacing="0" style="margin-left: 6px; height: 12px">
				<thead>
					<tr>
						<th rowspan="2">S.N.</th>
						<th rowspan="2">SUBJECTS</th>
						<th rowspan="2">CREDIT HOUR</th>
						<th colspan="2" rowspan="1" height="0">OBTAINED GRADE</th>
						<th rowspan="2"><label>FINAL GRADE</label></th>
						<th rowspan="2">GRADE POINT</th>
						<th rowspan="2">REMARKS</th>
					</tr>
					<tr>
						<th rowspan="1">TH</th>
						<th rowspan="1">PR</th>
					</tr>
				</thead>
				<tbody>
					@foreach($student['mark'] as $subjectmark)
					<tr>
						
						<td style="text-align: center;">{{$loop->index + 1}}</td>
						<td style="text-align: center;" width="45%">{{strtoupper($subjectmark['name'])}}</td>
						<td style="text-align: center;">{{$subjectmark['credithour']}}</td>
						<td  style="text-align: center;">{{$subjectmark['theorygrade']}}</td>
						<td style="text-align: center;">{{$subjectmark['practicalgrade']}}</td>
						<td style="text-align: center;">{{$subjectmark['subjectgrade']}}</td>
						<td style="text-align: center;">{{$subjectmark['subjectgradepoint']}}</td>

						<td height="" width="10%"></td>
						
					</tr>
					@endforeach
					
				</tbody>
				<tfoot>
				<tr >
					<td style="text-align: center;" colspan="4" class="foot" height="4%"><span>
						<label>AVRAGE (GPA)</label> <label style="">{{$student['final']->grade}}</label></span></td>
						<td style="text-align: center;" colspan="3" class="foot">GRADE POINT <label>&nbsp;{{$student['final']->grade_point}}</label></td>
						<td style="text-align: center;border: 1;" class="foot"></td>
					</tr>
					</tfoot>
				</table>
				<br>
				<br>
				<br>
				<br>
				<div>
					<table width="105%" style="margin-left: 6px">
						<tr>
							<td style="text-align: justify;">
								<hr class="line1">
								<label>PREPARED BY</label><br>
								<label>(HEAD TEACHER) </label>
							</td>
							<td>
								<hr class="line2">
								<label>REFERRED BY</label><br>
								<label>(RESOURCE PERSON)</label>
							</td>
							<td>
								<hr class="line3">
								<label>CHECKED BY</label><br>
								<label>(SECTION OFFICER)</label>
							</td>
							<td valign="bottom">
								<hr class="line4">
								<label>APPROVED BY</label><br>
								<label>(CHIEF ADMINISTRATIVE <br> OFFICER)</label>
							</td>
						</tr>
					</table>
				</div>
				<br>
				<div >
					&nbsp; &nbsp;DATE OF ISSUE: &nbsp;{{$today}}
				</div>
			</div>
		
@if(!$loop->last)
<div style="page-break-after: always;"></div>
@endif

@endforeach
</body>
</html>