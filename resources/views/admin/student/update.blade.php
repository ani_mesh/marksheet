@extends('layouts.new')

@section('content')
@include('admin.includes.error')
<!--banner-->	
<div class="banner">
	<h2>
	<a href="#">Home</a>
	<i class="fa fa-angle-right"></i>
	<span>Edit Student</span>
	</h2>
</div>
<!--//banner-->
<div class="content-top">
	<div class="grid-1 ">
				
	<div class="grid-form1">
 	<h3 id="forms-example" class="">Add Student</h3>
	<form action="{{route('student.update', ['student_id'=>$student->id])}}" method="post" enctype="multipart/form-data">
 			{{ csrf_field() }}
 		<div class="form-group">
		   	<label for="exampleInputEmail1">Roll Number</label>
		    <input type="text" class="form-control" name="roll" id="exampleInputEmail1" placeholder="Roll Number" value="{{$student->rollno}}" required>
  		</div>
	  <div class="form-group">
	    <label for="exampleInputEmail1">First Name</label>
	    <input type="text" class="form-control" name="firstname" id="exampleInputEmail1" value="{{$student->firstname}}" placeholder="First Name" required>
	  </div>
	  	<div class="form-group">
		    <label for="exampleInputPassword1">Middle Name</label>
		    <input type="text" class="form-control" name="middlename" value="{{$student->middlename}}" id="" placeholder="Middle Name (Optional)">
	  	</div>
   		<div class="form-group">
   			<label for="exampleInputPassword1">Last Name</label>
		    <input type="text" class="form-control" name="lastname" value="{{$student->lastname}}" id="" placeholder="Last Name" required>
  		</div>
  		<div class="form-group">
		   <label for="exampleInputPassword1">BirthDate in A.D.</label>
		    <input type="text" class="form-control" name="birthdate_ad" value="{{$student->birthdate_ad}}" id="" placeholder="BirthDate in A.D." required>
  		</div>
	  	<div class="form-group">
		   <label for="exampleInputPassword1">BirthDate in B.S.</label>
		    <input type="text" class="form-control" name="birthdate_bs" value="{{$student->birthdate_bs}}" id="" placeholder="BirthDate in B.S." required>
	  	</div>

  		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>
			</div>
		
		</div>
	

@stop
@section('styles')


@stop
