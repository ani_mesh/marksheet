<?php

namespace App\Http\Controllers\Subject;

use App\Http\Controllers\Controller;
use App\Schoolclass;
use App\Subject;
use Illuminate\Http\Request;
use Session;

class SubjectController extends Controller
{
    //function to return to view with all the subjects
    public function index()
    {
        //get all the subject
        $subjects = Subject::all();
        $classes  = Schoolclass::all();
        // dd($subjects->first()->sclass);
        return view('admin.Subject.index')->with('subjects', $subjects)
            ->with('classes', $classes);
    }

    public function create()
    {
        //get class of the student
        $class = Schoolclass::all()->sortby('name');

        //check if any class exist
        if (count($class) < 1)
        {
            Session::flash('nope', 'Add Class First');
            return redirect()->route('class.create');
        }

        return view('admin.Subject.create')->with('classes', $class);
    }

    public function store(Request $request)
    {
        //get all the request
        $data = $request->except('_token', 'class_id');
        // dd(empty($data['subject1']));
         // dd($data);
        //check for any empty inputs
        foreach ($data as $key => $value)
        {
            // dd($value['practical'] == 0);
            if (empty($value['name']) || $value['theory'] <= 0)
            {
                Session::flash('nope', 'Dont send empty inputs');
                return back();
            }
            if ($value['practical'] !== null && $value['practical'] <= 0) {
                 Session::flash('nope', 'Dont send value less than 1');
                return back();
            }
        }

        $class_id = $request->class_id;

        try {
            \DB::beginTransaction();
            // dd($data);
            foreach ($data as $key => $value)
            {
                $subject[] = Subject::create([
                    'name'       => $value['name'],
                    'totalmarks' => $value['theory'] + $value['practical'],
                    'practicalmarks' => $value['practical'],
                    'theorymarks'=>$value['theory'],
                    'credit_hours'=> $value['credit'],
                    'class_id'   => $class_id,
                ]);
            }
            \DB::commit();
            Session::flash('success', 'Subjects added successfully!');
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            Session::flash('error', 'Oops! Error Adding Subjects');
        }
        //saving each subject

        // Session::flash('success','Your Subject is saved');
        return back();
    }

    //filter subject with class
    public function filtersubject(Request $request)
    {
        $class = Schoolclass::find($request->class_id);
        $subjects = $class->subjects;
        return $subjects;
    }

    public function edit($subject_id)
    {
        $subject = Subject::findorFail($subject_id);
        // dd($subject);
        return view('admin.Subject.edit')->with('subject',$subject);
    }

    public function update(Request $request,$subject_id)
    {
        // dd($request->all());
        if (!$request->name || $request->theorymarks <= 0) {
            Session::flash('nope','Something Went wrong');
            return back();
        }

        if ($request->practicalmarks !== null && $request->practicalmarks <= 0) {
            Session::flash('nope','Something Went wrong');
            return back();
        }

        try {
            \DB::beginTransaction();
            $subject = Subject::find($subject_id);
            $subject->name = $request->name;
            $subject->credit_hours = $request->credit;
            $subject->theorymarks = $request->theorymarks;
            if ($subject->practicalmarks) {
                $subject->practicalmarks = $request->practicalmarks;
            }else{
                $subject->practicalmarks = null;
            }
            
            $subject->save();
            \DB::commit();
            Session::flash('success','Subject Updated succesfully');
        } catch (Exception $e) {
            Session::flash('nope','Something Went wrong');
        }
        return back();
    }
}
